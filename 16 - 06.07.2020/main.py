from flask import Flask, render_template, request, make_response

app = Flask( __name__ )

@app.route( "/" )
def index():
    name = "Bostjan"
    current_year = "2020"

    cities = [ "Vienna", "Maribor", "Ljubljana", "Celje" ]

    return render_template(
        "index.html",
        name=name,
        current_year=current_year,
        cities=cities
    )

@app.route( "/optout", methods=[ "GET" ] )
def optout():
    response = make_response( render_template( "optout.html" ) )
    response.set_cookie( "user_name", expires=0 )

    return response

@app.route( "/about", methods=[ "GET", "POST" ] )
def about():
    if request.method == "GET":
        user_name = request.cookies.get( "user_name" )
        return render_template( "about.html", user_name=user_name )
    elif request.method == "POST":
        name = request.form.get( "contact-name" )
        surname = request.form.get( "contact-surname" )
        message = request.form.get( "contact-message" )

        response = make_response( render_template( "success.html", name=name ) )
        response.set_cookie( "user_name", name )

        return response

@app.route( "/contact", methods=[ "POST" ] )
def contact():
    contact_name = request.form.get( "contact-name" )
    contact_surname = request.form.get( "contact-surname" )
    contact_message = request.form.get( "contact-message" )

    return render_template( "success.html" )



@app.route( "/portfolio" )
def portfolio():
    return render_template( "portfolio.html" )

if __name__ == '__main__':
    app.run()