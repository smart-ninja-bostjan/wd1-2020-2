from flask import Flask, render_template, redirect, url_for, request, make_response
from models import db, User

app = Flask( __name__ )
db.create_all()

@app.route( "/" )
def index():
    email = request.cookies.get( "email" )

    if email:
        user = db.query( User ).filter_by( email = email ).first()
    else:
        user = None

    return render_template( "index.html", user = user )


@app.route( "/login", methods=[ "POST" ] )
def login():
    name = request.form.get( "user-name" )
    email = request.form.get( "user-email" )

    user = User( name=name, email=email )

    db.add( user )
    db.commit()

    response = make_response( redirect( url_for( 'index' ) ) )
    response.set_cookie( "email", email )

    return response

if __name__ == '__main__':
    app.run()

# 1. Relational databases - SQLite, MySQL, Oracle, MSSQL ...
#       SQL Language - Structured Query Language
#       ORM -> Object Relationship Mapping
# 2. Non-Relational databases - MongoDB, CouchDB ...
#       No-SQL - Without a language
# 3. Graph based databases - DGraph, ...
#       Very specific