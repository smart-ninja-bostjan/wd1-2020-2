"""

Function in Python:

def functionName( ... ):
    # insert code

Function call:

functionName( ... )

"""

from functions import km_to_miles

def sayHello():
    print( "Hello!" )

sayHello()
sayHello()
sayHello()

# ... -> arguments of a function
# We separate arguments with a comma - ,
# return statement ->
def sumTwoNumbers( a, b ):
    c = a + b
    return c

sumOfTwo = sumTwoNumbers( 5, 10 )
print( sumOfTwo )

n1 = 15
n2 = 100

sumOfTwo = sumTwoNumbers( n1, n2 )
print( sumOfTwo )

miles = km_to_miles( 15 )
print( miles )

miles = km_to_miles( km = 100 )
print( miles )

name = "Bostjan"
surname = "Cigan"
age = "33"

sentence = "My name is: " + name + ", my surname is: " + surname + " and my age is " + age
print( sentence )

sentence = "My name is {0}, my surname is {1} and I am {2} years old".format( name, surname, age )
print( sentence )

def cubeMe( number ):
    cube = number * number * number
    return cube

number = 10
cube = cubeMe( number )
print( cube )
print( cubeMe( 300 ) )

def stepCounter( distance, stepLength ):
    numberOfSteps = int( distance / stepLength )
    return numberOfSteps

distance = 10000
stepLength = 100

numberOfSteps = stepCounter( distance, stepLength )
print( numberOfSteps )

numberOfSteps = stepCounter( distance=200, stepLength=10 )

a = 300
b = 500

numberOfSteps = stepCounter( distance=a, stepLength=b)