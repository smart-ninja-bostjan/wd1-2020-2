"""
battleship = [
    [ "R", False, False ],
    [ False, False, False ],
    [ False, False, "B" ]
]
"""

def generateBattlefield( size ):
    battlefield = []
    for i in range( 0, size ):
        battlefield.append( [ False ] * size )
    return battlefield

def placeShip( battlefield, x, y ):
    battlefield[ x ][ y ] = "R"

fieldSize = int( input( "Game size?" ) )
gameField = generateBattlefield( fieldSize )

while True:
    x = int( input( "X position: " ) )
    y = int( input ( "Y position: " ) )
    placeShip( gameField, x, y )
    print( gameField )
    # Randomly place ships for BLUE player which is PC
    # PC randomly picks x, y to hit RED player