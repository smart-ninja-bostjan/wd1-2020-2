import datetime
import json
import random

"""
#     0   1   2   3    4
a = [ 5, 10, -5, 20, 100 ]
b = a[ 1:3 ]
c = a[ 1: ]
d = a[ :4 ]
print( d )
"""

# a[ FIRST NUMBER : LAST NUMBER ] -> od do
# a [ : LAST_NUMBER ] -> od zacetka do last number
# a[ FIRST_NUMBER: ] -> od first number do konca

"""
dictionary = {}
dictionary[ 'variable' ] = 'spremenljivka'
dictionary[ 'space' ] = 'presledek'
dictionary[ 'backspace' ] = 'vracalka'
dictionary[ 'shift' ] = 'dvigalka'

print( dictionary )

print( dictionary[ 'shift' ] )
"""
secret = random.randint(1, 30)
attempts = 0

# a = []
# a.append( 1 )

person = {
    "name": "Bostjan",
    "surname": "Cigan",
    "address": "Somewhere unknown 7",
    "age": 33
}

person[ "age" ] = 80

print( person )

# Shopping cart = []
# Shopping cart items = {}

# { "name": "Persil", "price": 10.25, "quantity": 3 }

with open( "score_file.txt", "r" ) as score_file:
    score_list = json.loads( score_file.read() )
    # What about division with 0?

    for score_dict in score_list:
        print(str(score_dict["attempts"]) + " attempts, date: " + score_dict.get("date"))

    """
    if len( score_list ) > 0:
        # 1. route
        sumOfAttempts = 0
        for number in score_list:
            sumOfAttempts = sumOfAttempts + number
        average = sumOfAttempts / len( score_list )

        # 2. route
        average = sum( score_list ) / len( score_list )
        print( "Average attempts so far: " + str( int( average ) ) )
    else:
        print( "Keep up the score!" )
    """

    """
    score_list.sort()
    print( score_list[ :3 ] )
    # [ 1: ] -> od 1 do konca
    # [ :3 ] -> od zacetka do 3jega elementa
    # [ 1:3 ] -> od 1 do 3
#    print( "Top scores: " + str( score_list ) )
    """

while True:
    guess = int(input("Guess the secret number (between 1 and 30): "))
    attempts += 1

    if guess == secret:
        print("You've guessed it - congratulations! It's number " + str(secret))
        print("Attempts needed: " + str(attempts))

        attempt = {
            "attempts": attempts,
            "date": str( datetime.datetime.now() )
        }

        score_list.append( attempt )

        with open( "score_file.txt", "w" ) as score_file:
            score_file.write( json.dumps( score_list ) )
        break
    elif guess > secret:
        print("Your guess is not correct... try something smaller")
    elif guess < secret:
        print("Your guess is not correct... try something bigger")