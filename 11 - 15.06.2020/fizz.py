
while True:
    # 1. Read input from user ( 1 - 100 )
    # 2. Is it from 1 - 100?
    # 2.1 If it is 1 - 100:
    #       FizzBuzz
    # 3. Print Would you like to continue? (Y / N)
    # 3.1 Y -> ne nardis nic
    # 3.2 N -> break
    number = int( input( "Enter number: " ) )
    if number > 0 and number <= 100:
        # FizzBuzz code
        print( "I shall fizzbuzz here!" )
    else:
        print( "The number is not between 1 and 100." )
    continueGame = input( "Would you like to continue? (Y / N )" )
    if continueGame == "N":
        break