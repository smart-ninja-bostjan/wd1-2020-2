import random

def generateBattlefield( size ):
    battlefield = []
    for i in range( 0, size ):
        battlefield.append( False )
    return battlefield

def putShips( battlefield, ships ):
    for i in range( 0, ships ):
        shipPosition = random.randint( 0, ships )
        battlefield[ shipPosition ] = True

def hitShip( battlefield, position ):
    if battlefield[ position ]:
        print( "You sank my ship!" )
        battlefield[ position ] = False

def gameEnd( battlefield ):
    for ship in battlefield:
        if ship:
            return False
    return True

battleSize = int( input( "Enter size of battlefield: " ) )
shipsNumber = int( input( "Enter size of ship fleet: " ) )

battlefield = generateBattlefield( battleSize )
putShips( battlefield, shipsNumber )

while True:
    position = int( input( "Where to hit?" ) )
    hitShip( battlefield, position )
    gameend = gameEnd( battlefield )
    if gameend:
        print( "You sank all my ships!" )
        break

