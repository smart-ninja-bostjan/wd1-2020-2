import random

secret = random.randint( 1, 30 )

guess = False

"""
while guess != secret:
    guess = int(input("Guess the secret number (between 1 and 30): "))

    if guess == secret:
        print("You've guessed it - congratulations! It's number 22.")
    else:
        print("Sorry, your guess is not correct... The secret number is not " + str(guess))
"""

"""
    secret  guess
0.  22      False    -> 22 != False
1.  22      1        -> 22 != 1
2.  22      30       -> 22 != 30
3.  22      22       -> 22 != 22      
"""

# for variable in range( number ) -> number represents
# number of times the code in the loop gets executed

# BREAK -> forcefully break loop

"""
for x in range( 5 ):
    guess = int(input("Guess the secret number (between 1 and 30): "))

    if guess == secret:
        print("You've guessed it - congratulations! It's number 22.")
        break
    else:
        print("Sorry, your guess is not correct... The secret number is not " + str(guess))
"""

while True:
    guess = int(input("Guess the secret number (between 1 and 30): "))

    if guess == secret:
        print("You've guessed it - congratulations! It's number 22.")
        break
    elif( guess < secret ):
        print( "Guess higher!" )
    else: # A je potrebn tuki prevert guess > secret
        print( "Guess lower!" )