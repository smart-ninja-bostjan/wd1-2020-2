import random

# We need to do this 7 times!

l1 = False
l2 = False
l3 = False

# l1 -> Nam je vseen
# l2 -> l1 == l2
# l3 -> l1, l2 == l3
# l4 -> l1, l2, l3 == l4
# ... l7

loto = False

# L1, L2, L3 -> False -> None
# while not L1 ali L2 ali L3
while not l1 or not l2 or not l3:
    loto = random.randint( 1, 4 )
    if not l1:
        l1 = loto
    elif not l2:
        if l1 != loto: # 4, 4
            l2 = loto
    elif not l3:
        if l1 != loto and l2 != loto:
            l3 = loto

print( l1 )
print( l2 )
print( l3 )