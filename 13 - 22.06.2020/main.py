class Player():
    def __init__( self, first_name, last_name, height_cm, weight_kg ):
        self.first_name = first_name
        self.last_name = last_name
        self.height_cm = height_cm
        self.weight_kg = weight_kg

    def weight_to_lbs(self):
        lbs = self.weight_kg * 2.20462262
        return lbs


class FootbalPlayer( Player ):
    def __init__( self, first_name, last_name, height_cm, weight_kg,
                  goals,
                  yellow_cards,
                  red_cards):
        super().__init__(
            first_name=first_name,
            last_name=last_name,
            height_cm=height_cm,
            weight_kg=weight_kg
        )
        self.red_cards = red_cards
        self.yellow_cards = yellow_cards
        self.goals = goals

class BasketballPlayer( Player ):
    def __init__( self, first_name, last_name, height_cm, weight_kg,
                  points,
                  rebounds,
                  assists):
        super().__init__(
            first_name=first_name,
            last_name=last_name,
            height_cm=height_cm,
            weight_kg=weight_kg
        )
        self.points = points
        self.rebounds = rebounds
        self.assists = assists

lebron = BasketballPlayer(
    first_name="Lebron",
    last_name="James",
    height_cm=210,
    weight_kg=90,
    points=100,
    rebounds=5,
    assists=5
)

doncic = BasketballPlayer(
    first_name="Luka",
    last_name="Doncic",
    height_cm=210,
    weight_kg=90,
    points=100,
    rebounds=5,
    assists=5
)

print( lebron.first_name )
print( doncic.assists )

players = [ lebron, doncic ]

for player in players:
    pl = "{0} {1}".format( player.first_name, player.last_name )
    print( pl )

doncic_weight_lbs = doncic.weight_to_lbs()
print( doncic_weight_lbs )