from flask import Flask, render_template, redirect, url_for, request, make_response
from models import db, User

app = Flask( __name__ )
db.create_all()

translations = {
    "en": {
        "greeting": "Hello we salute you from the other side!"
    },
    "sl": {
        "greeting": "Pozdravljeni iz druge strani!"
    }
}

@app.route( "/" )
def index():
    language = request.cookies.get( "language" )

    if not language or not language in translations:
        language = "sl"

    return render_template( "index.html", translations=translations[ language ] )

@app.route( "/language/<language>", methods=[ "GET" ] )
def translate( language ):
    response = make_response( redirect( url_for('index' ) ) )

    if language in translations:
        response.set_cookie( "language", language )

    return response

if __name__ == '__main__':
    app.run()