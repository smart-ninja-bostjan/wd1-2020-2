import random
import sys

secret = random.randint( 1, 30 )

attempts = 0

"""
1. Read score.txt and read the biggest score so far
2. In a loop ask the user to guess the number
        - Attempts -> increase every time the user is in the loop
        - If user has guessed:
            Check if attempts is smaller than step 1
                If smaller, write new attempt in file
        - If not guessed, give hint
"""

with open( "score.txt", "r" ) as score_file:
    best_score = score_file.read()
    if best_score == "FIRST_GAME":
        print( "Hey this is insane! Your first time!" )
        best_score = sys.maxsize
    else:
        best_score = int( best_score )
        print( "Top score so far: " + str( best_score ) )

while True:
    guess = int( input( "Enter number: " ) )
    attempts = attempts + 1 # attempts += 1

    if guess == secret:
        print( "You guessed it!" )
        print( "You guessed in " + str( attempts ) )
        if attempts < best_score:
            with open( "score.txt", "w" ) as score_file:
                score_file.write( str( attempts ) )
        break
    elif guess < secret:
        print( "Guess needs to be bigger!" )
    else:
        print( "Guess needs to be lower!" )