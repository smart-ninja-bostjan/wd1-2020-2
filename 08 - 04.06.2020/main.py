### Primitive data types ###
### Integer, Double, Float, Boolean, String ###

# Integer, variable type
x = 5
y = 10

# variableName = ...

print( x )
print( y )

# Double, Float, variable type
z = 5.1234561231
print( z )

# String, variable type
text = "Hello planet! We are here!"

print( text )

# -, *, /, + -> arithmetic operations with numbers only!!!!
sumOfNumbers = x + y + z - 10 + 3 / 2
print( sumOfNumbers )

# Concatenation of strings
k = "Hello"
j = " world!"

allTogether = k + j
print( allTogether )

# Boolean variable type
doorOpen = True
doorClosed = False

# Nothing variable type
nothing = None

print( nothing )

#userName = input( "Please tell me your name: " )
#print( userName )

# Everything we read with input is a string!!!!
numberOne = input( "Enter first number: " )
numberTwo = input( "Enter second number: " )

sumOfUserNumbers = numberOne + numberTwo

print( sumOfUserNumbers )

# Casting - type casting!
# int( "30" ) -> 30
numberOne = int( numberOne )
numberTwo = int( numberTwo )

sumOfUserNumbers = numberOne + numberTwo

print( sumOfUserNumbers )

# IF ELSE STATEMENT

number = 8

if number > 9:
    print( "Hello!" )
elif number > 8:
    print( "More than 8!" )
elif number > 7:
    print( "More than 7!" )
else:
    print( "Nothing!" )
