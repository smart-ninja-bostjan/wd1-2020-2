import os
import pytest

from main import app, db, calculate

@pytest.fixture
def client():
    app.config[ 'TESTING' ] = True
    os.environ[ "DATABASE_URL" ] = "sqlite:///:memory:"
    client = app.test_client()

    cleanup()

    db.create_all()

    yield client

def cleanup():
    db.drop_all()

def test_index_not_logged_in( client ):
    response = client.get( "/" )
    assert b'Enter your name' in response.data

def test_index_logged_in( client ):
    client.post( '/login', data={
        "user-name": "bostjantest",
        "user-password": "geslo",
        "user-email": "nekdo@nekdo.com"
    }, follow_redirects=True )
    response = client.get( "/" )
    assert b'Enter your guess' in response.data

def test_calculate( client ):
    assert calculate( 3 ) == 9
    assert calculate( 0 ) == 0
    assert calculate( -3 ) == 9